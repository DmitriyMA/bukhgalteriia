<?php

namespace app\controllers;

use Yii;
use app\models\OrdersBuy;
use app\models\OrdersBuySearch;
use app\models\Orders;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * OrdersBuyController implements the CRUD actions for OrdersBuy model.
 */
class OrdersBuyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdersBuy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersBuySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdersBuy model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdersBuy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdersBuy();
        $model->date_buy = date('Y-m-d H:i:s');
        if (isset($_GET['id_orders']))
            $model->id_orders = $_GET['id_orders'];

        $model_o = Orders::find()->all();
        $orders = ArrayHelper::map($model_o, 'id_orders', 'product');

        $post = Yii::$app->request->post();
        if (isset($post["OrdersBuy"]['quantity']))
        {
            // $orders_quantity = 0;
            $order = Orders::findOne($post['OrdersBuy']['id_orders']);
            $orders_quantity = $order->quantity;

            // foreach ($model_o as $key => $value) {
            //     if ($value->id_orders == $post['OrdersBuy']['id_orders'])
            //     {
            //         $orders_quantity = $value->quantity;
            //         break;
            //     }
            // }
            $ob_quantity = (new \yii\db\Query())
                            ->from('orders_buy')
                            ->where('id_orders='.$post['OrdersBuy']['id_orders'])
                            ->sum('quantity');
            if (is_null($ob_quantity)) $ob_quantity = 0;

            if (($ob_quantity+$post['OrdersBuy']['quantity']) > $orders_quantity)
            {
                $model->load(Yii::$app->request->post());
                return $this->render('create', [
                    'model' => $model,
                    'orders' => $orders,
                    'error' => 'Количество товаров в продажах превышает количества товаров в покупках!!!',
                ]);
            }
            if (($ob_quantity+$post['OrdersBuy']['quantity']) == $orders_quantity)
            {
                $order->status = 3;
                $order->save();    
            }
            if (($ob_quantity+$post['OrdersBuy']['quantity']) < $orders_quantity)
            {
                $order->status = 0;
                $order->save();    
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_orders_buy]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'orders' => $orders,
                'error' => '',
            ]);
        }
    }

    /**
     * Updates an existing OrdersBuy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model_o = Orders::find()->all();
        $orders = ArrayHelper::map($model_o, 'id_orders', 'product');

        $post = Yii::$app->request->post();
        if (isset($post["OrdersBuy"]['quantity']))
        {
            $order = Orders::findOne($post['OrdersBuy']['id_orders']);
            $orders_quantity = $order->quantity;

            $ob_quantity = (new \yii\db\Query())
                            ->from('orders_buy')
                            ->where('id_orders='.$post['OrdersBuy']['id_orders'])
                            ->sum('quantity');
            if (is_null($ob_quantity)) $ob_quantity = 0;

            if (($ob_quantity+$post['OrdersBuy']['quantity']) > $orders_quantity)
            {
                $model->load(Yii::$app->request->post());
                return $this->render('create', [
                    'model' => $model,
                    'orders' => $orders,
                    'error' => 'Количество товаров в продажах превышает количества товаров в покупках!!!',
                ]);
            }
            if (($ob_quantity+$post['OrdersBuy']['quantity']) == $orders_quantity)
            {
                $order->status = 3;
                $order->save();    
            }
            if (($ob_quantity+$post['OrdersBuy']['quantity']) < $orders_quantity)
            {
                $order->status = 0;
                $order->save();    
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_orders_buy]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'orders' => $orders,
                'error' => '',
            ]);
        }       

    }

    /**
     * Deletes an existing OrdersBuy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrdersBuy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdersBuy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdersBuy::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
