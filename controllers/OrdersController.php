<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\Users;
use app\models\Wallet;
use app\models\Status;
use app\models\WalletLog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();
        $model->date_buy = date("Y-m-d H:i:s");

        $model_u = Users::find()->all();
        $users = ArrayHelper::map($model_u, 'id_users', 'name');

        // $model_w = Wallet::find()->all();
        $model_w = (new \yii\db\Query())
                    ->select(["wallet.id_wallet", "concat(users.name, ' - ', wallet.wallet) as wallet"])
                    ->from('wallet')
                    ->leftJoin('users', 'users.id_users=wallet.id_users')
                    ->indexBy('id_wallet')
                    ->all();

        $wallets = ArrayHelper::map($model_w, 'id_wallet', 'wallet');

        $model_s = new Status();
        $statuses = $model_s::listStatus();
        // var_dump($statuses);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            # отнимаем с кошелька деньги
            $post = Yii::$app->request->post();
            $wallet = Wallet::findOne($post['Orders']['id_wallet']);
            $summa_old = $wallet->summa;
            $wallet->summa = $summa_old - $post['Orders']['summa'];
            $wallet->save();

            $wallet_log = new WalletLog();
            $wallet_log->id_wallet = $post['Orders']['id_wallet'];
            $wallet_log->id_users = $post['Orders']['id_users'];
            $wallet_log->date = date("Y-m-d H:i:s");
            $wallet_log->difference = $post['Orders']['summa'];
            $wallet_log->summa_old = $summa_old;
            $wallet_log->summa_new = $summa_old - $post['Orders']['summa'];
            $wallet_log->source = 'orders_create';
            $wallet_log->id_source = $model->id_orders;
            $wallet_log->save();

            return $this->redirect(['view', 'id' => $model->id_orders]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'users' => $users,
                'wallets' => $wallets,
                'statuses' => $statuses,
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $summa = $model->summa;

        $model_u = Users::find()->all();
        $users = ArrayHelper::map($model_u, 'id_users', 'name');

        // $model_w = Wallet::find()->all();
        $model_w = (new \yii\db\Query())
                    ->select(["wallet.id_wallet", "concat(users.name, ' - ', wallet.wallet) as wallet"])
                    ->from('wallet')
                    ->leftJoin('users', 'users.id_users=wallet.id_users')
                    ->indexBy('id_wallet')
                    ->all();

        $wallets = ArrayHelper::map($model_w, 'id_wallet', 'wallet');

        $model_s = new Status();
        $statuses = $model_s::listStatus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            # отнимаем с кошелька деньги
            $post = Yii::$app->request->post();
            if ($summa != $post['Orders']['summa'])
            {
                $wallet = Wallet::findOne($post['Orders']['id_wallet']);
                $summa_old = $wallet->summa;
                $wallet->summa = $summa_old - $post['Orders']['summa'] + $summa;
                $wallet->save();

                $wallet_log = new WalletLog();
                $wallet_log->id_wallet = $post['Orders']['id_wallet'];
                $wallet_log->id_users = $post['Orders']['id_users'];
                $wallet_log->date = date("Y-m-d H:i:s");
                $wallet_log->difference = $post['Orders']['summa'];
                $wallet_log->summa_old = $summa_old;
                $wallet_log->summa_new = $summa_old - $post['Orders']['summa'] + $summa;
                $wallet_log->source = 'orders_update';
                $wallet_log->id_source = $model->id_orders;
                $wallet_log->save();
            }

            return $this->redirect(['view', 'id' => $model->id_orders]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'users' => $users,
                'wallets' => $wallets,
                'statuses' => $statuses,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
