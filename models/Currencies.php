<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currencies".
 *
 * @property integer $id_currensies
 * @property string $currency
 * @property string $short_currency
 * @property string $short_currency_en
 * @property string $currency_char
 *
 * @property Wallet[] $wallets
 */
class Currencies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency'], 'string', 'max' => 50],
            [['short_currency', 'short_currency_en'], 'string', 'max' => 5],
            [['currency_char'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_currencies' => 'Id Currensies',
            'currency' => 'валюта',
            'short_currency' => 'сокращение валютаы',
            'short_currency_en' => 'обозначение валюты',
            'currency_char' => 'символ валюты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallets()
    {
        return $this->hasMany(Wallet::className(), ['id_currencies' => 'id_currencies'])->inverseOf('idCurrencies');
    }
}
