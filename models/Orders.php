<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id_orders
 * @property integer $id_users
 * @property integer $id_wallet
 * @property string $date_buy
 * @property string $product
 * @property integer $quantity
 * @property double $price
 * @property double $summa
 * @property string $shop
 * @property string $shop_order
 * @property string $shop_track
 * @property integer $status
 *
 * @property Users $idUsers
 * @property Wallet $idWallet
 * @property OrdersBuy[] $ordersBuys
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_users', 'id_wallet', 'quantity', 'status'], 'integer'],
            [['date_buy'], 'safe'],
            [['price', 'summa'], 'number'],
            [['product'], 'string', 'max' => 255],
            [['shop', 'shop_order', 'shop_track'], 'string', 'max' => 25],
            [['id_users'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_users' => 'id_users']],
            [['id_wallet'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['id_wallet' => 'id_wallet']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_orders' => 'Id Orders',
            'id_users' => 'для кого покупался',
            'id_wallet' => 'кошелек покупки',
            'date_buy' => 'дата покупки',
            'product' => 'продукт',
            'quantity' => 'количество',
            'price' => 'цена за единицу',
            'summa' => 'сумма',
            'shop' => 'магазин покупки',
            'shop_order' => 'номер заказа в магазине',
            'shop_track' => 'трэк отслеживания',
            'status' => 'статус покупки',
            'name' => 'для кого покупался',
            'wallet' => 'кошелек покупки',
            'ob_quantity' => 'количество проданного',
            'ob_summa' => 'сумма продажи',
            'ob_summa_cur' => 'сумма в валюте',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsers()
    {
        return $this->hasOne(Users::className(), ['id_users' => 'id_users'])->inverseOf('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdWallet()
    {
        return $this->hasOne(Wallet::className(), ['id_wallet' => 'id_wallet'])->inverseOf('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersBuys()
    {
        return $this->hasMany(OrdersBuy::className(), ['id_orders' => 'id_orders'])->inverseOf('idOrders');
    }
}
