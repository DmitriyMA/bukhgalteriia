<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders_buy".
 *
 * @property integer $id_orders_buy
 * @property integer $id_orders
 * @property string $date_buy
 * @property integer $quantity
 * @property double $price
 * @property double $summa
 * @property double $summa_cur
 *
 * @property Orders $idOrders
 */
class OrdersBuy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_buy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_orders', 'quantity'], 'integer'],
            [['date_buy'], 'safe'],
            [['price', 'summa', 'summa_cur'], 'number'],
            [['id_orders'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['id_orders' => 'id_orders']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_orders_buy' => 'Id',
            'id_orders' => 'Покупка',
            'date_buy' => 'Дата продажи',
            'quantity' => 'Количество проданных',
            'price' => 'Цена продажи',
            'summa' => 'Сумма продажи',
            'summa_cur' => 'Сумма в валюте покупки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdOrders()
    {
        return $this->hasOne(Orders::className(), ['id_orders' => 'id_orders'])->inverseOf('ordersBuys');
    }
}
