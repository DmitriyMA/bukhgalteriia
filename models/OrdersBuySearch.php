<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrdersBuy;

/**
 * OrdersBuySearch represents the model behind the search form about `app\models\OrdersBuy`.
 */
class OrdersBuySearch extends OrdersBuy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_orders_buy', 'id_orders', 'quantity'], 'integer'],
            [['date_buy'], 'safe'],
            [['price', 'summa', 'summa_cur'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = OrdersBuy::find();
        $query = (new \yii\db\Query())
                    ->select('orders_buy.id_orders_buy, orders_buy.id_orders, orders_buy.date_buy, orders_buy.quantity, orders_buy.price, orders_buy.summa, orders_buy.summa_cur')
                    ->from('orders_buy')
                    ->leftJoin('orders', 'orders.id_orders=orders_buy.id_orders')
                    ->indexBy('id_orders_buy');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_orders_buy' => $this->id_orders_buy,
            'orders_buy.id_orders' => $this->id_orders,
            'date_buy' => $this->date_buy,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'summa' => $this->summa,
            'summa_cur' => $this->summa_cur,
        ]);

        return $dataProvider;
    }
}
