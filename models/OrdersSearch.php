<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_orders', 'id_users', 'id_wallet', 'quantity', 'status'], 'integer'],
            [['date_buy', 'product', 'shop', 'shop_order', 'shop_track'], 'safe'],
            [['price', 'summa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = Orders::find();
        $query = (new \yii\db\Query())
                 ->select('orders.id_orders, users.name, wallet.wallet, date_buy, product, quantity, price, orders.summa, shop, shop_order, shop_track, status, ob.ob_quantity, ob.ob_summa, ob.ob_summa_cur, (ob.ob_summa_cur - orders.summa) as benefit')
                 ->from('orders')
                 ->leftJoin('users', 'users.id_users=orders.id_users')
                 ->leftJoin('wallet', 'wallet.id_wallet=orders.id_wallet')
                 ->leftJoin('(SELECT id_orders, sum(quantity) as ob_quantity, sum(summa) as ob_summa, sum(summa_cur) as ob_summa_cur FROM orders_buy GROUP BY id_orders) as ob', 'orders.id_orders=ob.id_orders')
                 ->indexBy('id_orders');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_orders' => $this->id_orders,
            'id_users' => $this->id_users,
            'id_wallet' => $this->id_wallet,
            'date_buy' => $this->date_buy,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'summa' => $this->summa,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'product', $this->product])
            ->andFilterWhere(['like', 'shop', $this->shop])
            ->andFilterWhere(['like', 'shop_order', $this->shop_order])
            ->andFilterWhere(['like', 'shop_track', $this->shop_track]);

        return $dataProvider;
    }
}
