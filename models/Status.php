<?php

namespace app\models;

use Yii;
use \yii\helpers\ArrayHelper;


class Status
{

    // private $status = array();

    public function listStatus ()
    {
        return ['0' => 'Куплен',
                '1' => 'Отменен',
                '2' => 'Для личных целей',
                '3' => 'Продан полностью'];
    }
}
