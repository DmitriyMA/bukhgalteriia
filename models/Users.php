<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id_users
 * @property string $login
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $action
 *
 * @property Wallet[] $wallets
 * @property WalletLog[] $walletLogs
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action'], 'integer'],
            [['login', 'name', 'email', 'password'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_users' => 'Id Users',
            'login' => 'Login',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'action' => 'Action',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWallets()
    {
        return $this->hasMany(Wallet::className(), ['id_users' => 'id_users'])->inverseOf('idUsers');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletLogs()
    {
        return $this->hasMany(WalletLog::className(), ['id_users' => 'id_users'])->inverseOf('idUsers');
    }
}
