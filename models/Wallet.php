<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wallet".
 *
 * @property integer $id_wallet
 * @property integer $id_users
 * @property string $wallet
 * @property integer $id_currencies
 * @property double $summa
 *
 * @property Users $idUsers
 * @property Currencies $idCurrencies
 * @property WalletLog[] $walletLogs
 */
class Wallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_users', 'id_currencies'], 'integer'],
            [['summa'], 'number'],
            [['wallet'], 'string', 'max' => 25],
            [['id_users'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_users' => 'id_users']],
            [['id_currencies'], 'exist', 'skipOnError' => true, 'targetClass' => Currencies::className(), 'targetAttribute' => ['id_currencies' => 'id_currencies']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_wallet' => 'Id',
            'id_users' => 'Cсылка на пользователя',
            'name' => 'Имя',
            'wallet' => 'Название кошелька',
            'short_currency' => 'Валюта',
            'summa' => 'Cумма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsers()
    {
        return $this->hasOne(Users::className(), ['id_users' => 'id_users'])->inverseOf('wallets');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCurrencies()
    {
        return $this->hasOne(Currencies::className(), ['id_currencies' => 'id_currencies'])->inverseOf('wallets');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletLogs()
    {
        return $this->hasMany(WalletLog::className(), ['id_wallet' => 'id_wallet'])->inverseOf('idWallet');
    }


    // public function search($params)
    // {
    //     $rows = (new \yii\db\Query())
    //             ->select(['id_wallet', 'id_users', 'wallet', 'id_currencies', 'summa'])
    //             ->from('wallet');

    //     return $rows->all();

    // }
}
