<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wallet_log".
 *
 * @property integer $id_wallet_log
 * @property integer $id_wallet
 * @property integer $id_users
 * @property string $date
 * @property double $difference
 * @property double $summa_old
 * @property double $summa_new
 * @property string $source
 *
 * @property Wallet $idWallet
 * @property Users $idUsers
 */
class WalletLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_wallet', 'id_users', 'id_source'], 'integer'],
            [['date'], 'safe'],
            [['difference', 'summa_old', 'summa_new'], 'number'],
            [['source'], 'string', 'max' => 25],
            [['id_wallet'], 'exist', 'skipOnError' => true, 'targetClass' => Wallet::className(), 'targetAttribute' => ['id_wallet' => 'id_wallet']],
            [['id_users'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_users' => 'id_users']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_wallet_log' => 'Id',
            'id_wallet' => 'Кошелек',
            'id_users' => 'Чья операция',
            'date' => 'Дата операции',
            'difference' => 'Действие',
            'summa_old' => 'Старая сумма',
            'summa_new' => 'Новая сумма',
            'source' => 'Источник',
            'id_source' => 'id источника',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdWallet()
    {
        return $this->hasOne(Wallet::className(), ['id_wallet' => 'id_wallet'])->inverseOf('walletLogs');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsers()
    {
        return $this->hasOne(Users::className(), ['id_users' => 'id_users'])->inverseOf('walletLogs');
    }
}
