<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WalletLog;

/**
 * WalletLogSearch represents the model behind the search form about `app\models\WalletLog`.
 */
class WalletLogSearch extends WalletLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_wallet_log', 'id_wallet', 'id_users'], 'integer'],
            [['date', 'source'], 'safe'],
            [['difference', 'summa_old', 'summa_new'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WalletLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_wallet_log' => $this->id_wallet_log,
            'id_wallet' => $this->id_wallet,
            'id_users' => $this->id_users,
            'date' => $this->date,
            'difference' => $this->difference,
            'summa_old' => $this->summa_old,
            'summa_new' => $this->summa_new,
        ]);

        $query->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
