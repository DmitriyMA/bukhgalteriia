<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Wallet;

/**
 * WalletSearch represents the model behind the search form about `app\models\Wallet`.
 */
class WalletSearch extends Wallet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_wallet', 'id_users', 'id_currencies'], 'integer'],
            [['wallet'], 'safe'],
            [['summa'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = Wallet::find();
        $query = (new \yii\db\Query())
                ->select(['id_wallet', 'users.name', 'wallet', 'currencies.short_currency', 'summa'])
                ->from('wallet')
                ->leftJoin('users', 'wallet.id_users = users.id_users')
                ->leftJoin('currencies', 'wallet.id_currencies = currencies.id_currencies')
                ->indexBy('id_wallet');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_wallet' => $this->id_wallet,
            'id_users' => $this->id_users,
            'id_currencies' => $this->id_currencies,
            'summa' => $this->summa,
        ]);

        $query->andFilterWhere(['like', 'wallet', $this->wallet]);

        return $dataProvider;
    }
}
