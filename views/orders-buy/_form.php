<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersBuy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-buy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'id_orders')->textInput() ?>
    <?= $form->field($model, 'id_orders')->dropDownList($orders, ['prompt' => 'Выберите покупку ...',]) ?>

    <?= $form->field($model, 'date_buy')->textInput() ?>
   
    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'summa')->textInput() ?>

    <?= $form->field($model, 'summa_cur')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="/web/assets/f5ccf1da/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#ordersbuy-quantity").on('keyup', function () {
            var q = $("#ordersbuy-quantity").val();
            var p = $("#ordersbuy-price").val();
            if (p != '') $("#ordersbuy-summa").val(p*q);
        });
        $("#ordersbuy-price").on('keyup', function () {
            var q = $("#ordersbuy-quantity").val();
            var p = $("#ordersbuy-price").val();
            if (q != '') $("#ordersbuy-summa").val(p*q);
        });
    });    
</script>