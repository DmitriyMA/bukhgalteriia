<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrdersBuy */

$this->title = 'Добавить продажу';
$this->params['breadcrumbs'][] = ['label' => 'Продажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if ($error != '') $error = "<span style='color: red;font-weight: bold;font-size: 16px;'>".$error."</span><br><br>";
?>
<div class="orders-buy-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $error ?>
    <?= $this->render('_form', [
        'model' => $model,
        'orders' => $orders,
    ]) ?>

</div>
