<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersBuy */

$this->title = 'Редактирование продажи: ' . $model->id_orders_buy;
$this->params['breadcrumbs'][] = ['label' => 'Продажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_orders_buy, 'url' => ['view', 'id' => $model->id_orders_buy]];
$this->params['breadcrumbs'][] = 'Редактирование';

if ($error != '') $error = "<span style='color: red;font-weight: bold;font-size: 16px;'>".$error."</span><br><br>";
?>
<div class="orders-buy-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'orders' => $orders,
    ]) ?>

</div>
