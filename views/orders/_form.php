<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'id_users')->textInput() ?>
    <?= $form->field($model, 'id_users')->dropDownList($users, ['prompt' => 'Выберите покупателя ...']); ?>

    <?//= $form->field($model, 'id_wallet')->textInput() ?>
    <?= $form->field($model, 'id_wallet')->dropDownList($wallets, ['prompt' => 'Выберете кошелек покупки ...']); ?>

    <?= $form->field($model, 'date_buy')->textInput() ?>

    <?= $form->field($model, 'product')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'summa')->textInput() ?>

    <?= $form->field($model, 'shop')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shop_order')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shop_track')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'status')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList($statuses); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="/web/assets/f5ccf1da/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#orders-quantity").on('keyup', function () {
            var q = $("#orders-quantity").val();
            var p = $("#orders-price").val();
            if (p != '') $("#orders-summa").val(p*q);
        });
        $("#orders-price").on('keyup', function () {
            var q = $("#orders-quantity").val();
            var p = $("#orders-price").val();
            if (q != '') $("#orders-summa").val(p*q);
        });
    });    
</script>
