<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_orders') ?>

    <?= $form->field($model, 'id_users') ?>

    <?= $form->field($model, 'id_wallet') ?>

    <?= $form->field($model, 'date_buy') ?>

    <?= $form->field($model, 'product') ?>

    <?php // echo $form->field($model, 'quantity') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'summa') ?>

    <?php // echo $form->field($model, 'shop') ?>

    <?php // echo $form->field($model, 'shop_order') ?>

    <?php // echo $form->field($model, 'shop_track') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
