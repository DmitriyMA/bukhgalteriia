<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Покупки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить покупку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_orders',
            'name',
            'wallet',
            'date_buy',
            'product',
            'quantity',
            'price',
            'summa',
            'shop',
            'shop_order',
            'shop_track',
            'status',
            'ob_quantity',
            'ob_summa',
            'ob_summa_cur',
            'benefit',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{view} {update} {delete} {createbuy} {list}',
             'buttons' => [
                'createbuy' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-check" title="Добавить продажу"></span>', '?r=orders-buy/create&id_orders='.$model['id_orders']);
                },
                'list' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-list" title="Список продаж"></span>', '?r=orders-buy/index&id_orders='.$model['id_orders']);
                },
             ],
            ],
        ],
        'rowOptions'=>function ($model, $key, $index, $grid){
            // $class=$index%2?'odd':'even';
            // var_dump($model['status']);
            $style = '';
            if ($model['status'] == 1) $style = 'background-color: #fc3f3f';
            elseif ($model['status'] == 2) $style = 'background-color: #fefe2c';
            elseif ($model['status'] == 3) $style = 'background-color: #3dfc3d';
            return [
                'key'=>$key,
                'index'=>$index,
                'style'=>$style
            ];
        },
    ]); ?>
</div>
