<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\WalletLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_wallet_log') ?>

    <?= $form->field($model, 'id_wallet') ?>

    <?= $form->field($model, 'id_users') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'difference') ?>

    <?php // echo $form->field($model, 'summa_old') ?>

    <?php // echo $form->field($model, 'summa_new') ?>

    <?php // echo $form->field($model, 'source') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
