<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WalletLog */

$this->title = 'Create Wallet Log';
$this->params['breadcrumbs'][] = ['label' => 'Wallet Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
