<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WalletLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wallet Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wallet Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_wallet_log',
            'id_wallet',
            'id_users',
            'date',
            'difference',
            // 'summa_old',
            // 'summa_new',
            // 'source',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
