<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WalletLog */

$this->title = 'Update Wallet Log: ' . $model->id_wallet_log;
$this->params['breadcrumbs'][] = ['label' => 'Wallet Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_wallet_log, 'url' => ['view', 'id' => $model->id_wallet_log]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wallet-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
