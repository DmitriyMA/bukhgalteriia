<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Wallet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="wallet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'id_users')->textInput() ?>
    
    <?= $form->field($model, 'id_users')->dropDownList($users, ['prompt' => 'Выберите пользователя...']) ?>

    <?= $form->field($model, 'wallet')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'id_currencies')->textInput() ?>
    <?= $form->field($model, 'id_currencies')->dropDownList($currencies, ['prompt' => 'Выберите валюту...']) ?>

    <?= $form->field($model, 'summa')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
