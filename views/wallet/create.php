<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Wallet */

$this->title = 'Создание кошелька';
$this->params['breadcrumbs'][] = ['label' => 'Кошельки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users,
        'currencies' => $currencies,
    ]) ?>

</div>
